package epamjava.concurrency.task1;

public class MatrixWriterThread implements Runnable {
    private int uid;
    private ConcurrentMatrix matrix;

    /**
     * MatrixWriterThread Constructor
     * @param uid unique id
     * @param matrix that this thread works with
     */
    public MatrixWriterThread(int uid, ConcurrentMatrix matrix) {
        this.uid = uid;
        this.matrix = matrix;
    }

    @Override
    public void run() {
        for (int i = 0; i < matrix.getNumberOfRows() && i < matrix.getNumberOfColumns(); i++) {
            if (!matrix.isLocked(i, i) && !matrix.isChanged(i, i)) {
                matrix.set(i, i, uid);
            }
        }
    }
}

package epamjava.concurrency.task1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Utilities {

    public static ConcurrentMatrix createRandomMatrixWithZeroDiagonal(int dimension) {
        ConcurrentMatrix matrix = new ConcurrentMatrix(dimension, dimension);

        Random random = new Random();
        for (int i = 0; i < matrix.getNumberOfRows(); i++) {
            for (int j = 0; j < matrix.getNumberOfColumns(); j++) {
                if (i != j) {
                    matrix.set(i, j, random.nextInt(10));
                }
            }
        }

        return matrix;
    }

    public static List<Thread> createMatrixWriterThreads(int numberOfThreads, ConcurrentMatrix matrix) {
        List<Thread> threads = new ArrayList<>();

        for (int i = 1; i < numberOfThreads + 1; i++) {
            threads.add(new Thread(new MatrixWriterThread(100 + i, matrix)));
        }

        return threads;
    }
}

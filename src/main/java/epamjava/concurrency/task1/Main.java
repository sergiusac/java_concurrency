package epamjava.concurrency.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        File file = new File("data\\matrix.txt");

        if (file.exists()) {
            try {
                Scanner scanner = new Scanner(file);
                int dimension = scanner.nextInt();
                int numberOfThreads = scanner.nextInt();

                boolean correctDimension = dimension >= 8 && dimension <= 12;
                boolean correctNumberOfThreads = numberOfThreads >= 4 && numberOfThreads <= 6;

                if (correctDimension && correctNumberOfThreads) {
                    LOGGER.info("Matrix Dimension: " + dimension);
                    LOGGER.info("Number of threads: " + numberOfThreads);

                    ConcurrentMatrix matrix = Utilities.createRandomMatrixWithZeroDiagonal(dimension);
                    List<Thread> threads = Utilities.createMatrixWriterThreads(numberOfThreads, matrix);
                    LOGGER.info("Initial Matrix: \n" + matrix);

                    for (Thread thread : threads) {
                        thread.start();
                    }
                    for (Thread thread : threads) {
                        thread.join();
                    }

                    LOGGER.info("Changed Matrix: \n" + matrix);
                }

                if (!correctDimension) {
                    LOGGER.error("Matrix Dimension (N) must be 8<=N<=12");
                }
                if (!correctNumberOfThreads) {
                    LOGGER.error("Number of Threads (M) must be 4<=M<=6");
                }

            } catch (InterruptedException | FileNotFoundException e) {
                LOGGER.error("error: ", e);
            }
        } else {
            LOGGER.info("Initialization data does not exist");
        }
    }
}

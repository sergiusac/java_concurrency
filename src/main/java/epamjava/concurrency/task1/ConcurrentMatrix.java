package epamjava.concurrency.task1;

public class ConcurrentMatrix {

    private ConcurrentMatrixElement[][] elements;
    private int numberOfRows;
    private int numberOfColumns;

    /**
     * ConcurrentMatrix Constructor
     * @param numberOfRows number of rows
     * @param numberOfColumns number of columns
     */
    public ConcurrentMatrix(int numberOfRows, int numberOfColumns) {
        elements = new ConcurrentMatrixElement[numberOfRows][numberOfColumns];
        for (int i = 0; i < numberOfRows; i++) {
            for (int j = 0; j < numberOfColumns; j++) {
                elements[i][j] = new ConcurrentMatrixElement(0, i, j);
            }
        }

        this.numberOfRows = numberOfRows;
        this.numberOfColumns = numberOfColumns;
    }

    /**
     * Returns an appropriate value from this matrix
     * @param i row number of a value
     * @param j column number of a value
     * @return value
     */
    public int get(int i, int j) {
        return elements[i][j].getValue();
    }

    /**
     * Sets an appropriate value in this matrix
     * @param i row number of value
     * @param j column number of value
     * @param value to set
     */
    public void set(int i, int j, int value) {
        elements[i][j].set(value, true);
    }

    /**
     * Indicates whether a value by indexes `i` and `j` has been changed
     * @param i row number of a value
     * @param j column number of a value
     * @return true if a value has been changed, otherwise false
     */
    public boolean isChanged(int i, int j) {
        return elements[i][j].isChanged();
    }

    /**
     * Indicates whether a value by indexes `i` and `j` has been locked by another thread
     * @param i row number of a value
     * @param j column number of a value
     * @return true if a value has been locked, otherwise false
     */
    public boolean isLocked(int i, int j) {
        return elements[i][j].isLocked();
    }

    /**
     * Returns number of rows
     * @return number of rows
     */
    public int getNumberOfRows() {
        return numberOfRows;
    }

    /**
     * Returns number of columns
     * @return number of columns
     */
    public int getNumberOfColumns() {
        return numberOfColumns;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();

        for (int i = 0; i < numberOfRows; i++) {
            buffer.append("|");
            for (int j = 0; j < numberOfColumns; j++) {
                buffer.append(' ').append(elements[i][j].getValue()).append(' ');
            }
            buffer.append("|\n");
        }

        return buffer.toString();
    }
}

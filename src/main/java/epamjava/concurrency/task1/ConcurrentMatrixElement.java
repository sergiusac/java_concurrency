package epamjava.concurrency.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ConcurrentMatrixElement {
    private int value;
    private AtomicBoolean changed;
    private int row, column;

    private final ReentrantLock lock = new ReentrantLock(true);

    private static final Logger LOGGER = LogManager.getLogger(ConcurrentMatrixElement.class);

    /**
     * Constructor with default value (false) for `changed` field
     * @param value represents the value that this object contains
     * @param row represents row number in a matrix
     * @param column represents column number in a matrix
     */
    public ConcurrentMatrixElement(int value, int row, int column) {
        this(value, false, row, column);
    }

    /**
     * Full constructor
     * @param value represents value that this object contains
     * @param changed indicates whether this element has been changed
     * @param row represents row number in a matrix
     * @param column represents column number in a matrix
     */
    public ConcurrentMatrixElement(int value, boolean changed, int row, int column) {
        this.value = value;
        this.changed = new AtomicBoolean(changed);
        this.row = row;
        this.column = column;
    }

    /**
     * Sets an appropriate value
     * @param value to set
     */
    public void set(int value, boolean exitIfWasAlreadyChanged) {
        try {
            lock.lock();

            if (exitIfWasAlreadyChanged) {
                if (isChanged()) {
                    return;
                }
            }

            this.value = value;
            this.changed.set(true);

            LOGGER.debug(String.format(
                    "Thread %s changed matrix[%d][%d] on %d",
                    Thread.currentThread().getName(), row, column, value
            ));

            // it is very hard to change the value
            TimeUnit.MILLISECONDS.sleep(1);
        } catch (InterruptedException e) {
            LOGGER.error("error: ", e);
        } finally {
            lock.unlock();
        }
    }

    /**
     *  Returns value that this object contains
     * @return value
     */
    public int getValue() {
        return value;
    }

    /**
     * Indicates whether this element has been changed
     * @return true if this element has been changed, otherwise false
     */
    public boolean isChanged() {
        return changed.get();
    }

    /**
     * Indicates whether this element has been locked by another thread
     * @return true if this element has been locked, otherwise false
     */
    public boolean isLocked() {
        return lock.isLocked();
    }

    /**
     * Returns row number in a matrix
     * @return row number
     */
    public int getRow() {
        return row;
    }

    /**
     * Returns column number in a matrix
     * @return column number
     */
    public int getColumn() {
        return column;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
